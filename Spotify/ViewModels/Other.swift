import Foundation

struct AlbumCollectionViewCellViewModel {
    let name: String
    let artistName: String
}

struct CategoryCollectionViewCellViewModel {
    let title: String
    let artworkURL: URL?
}

struct FeaturedPlaylistCellViewModel {
    let name: String
    let artworkURL: URL?
    let creatorName: String
}

struct PlaylistHeaderViewViewModel {
    let name: String?
    let ownerName: String?
    let description: String?
    let artworkURL: URL?
}

struct SearchResultDefaultTableViewCellViewModel {
    let title: String
    let imageURL: URL?
}

struct SearchResultSubtitleTableViewCellViewModel {
    let title: String
    let subtitle: String
    let imageURL: URL?
}
